import axios from "axios";
import {MPP_API, MPP_URL} from "../constants";
import {
    AddSubscription,
    AddSubsctiptionResponse, AsynchronousInitiationParameters,
    PaymentDetailsCard,
    ThreeDSecureParams, UserSubscription,
    VoucherResponseInfo
} from "../domain/model";

export interface MPPStatus {
    endpointAvailable: boolean
    databaseAvailable: boolean
    systemDateTime: string
    buildVersion: string
}

class MPPService {

    static colorDepthValues : { [key:string]: string } = {
        "1-1": "ONE_BIT",
        "2-3": "TWO_BITS",
        "4-7": "FOUR_BITS",
        "8-14": "EIGHT_BITS",
        "15-15": "FIFTEEN_BITS",
        "16-16": "SIXTEEN_BITS",
        "24-31": "TWENTY_FOUR_BITS",
        "32-47": "THIRTY_TWO_BITS",
        "48-128": "FORTY_EIGHT_BITS"
    }

    public guid? : string;

    private headers = {
        headers: {
            'x-tokenid': '6045D1F2CDAE487CBB809E1810C40130',
            'x-version' : '11.16.0',
        }
    }

    async getStatus() : Promise<MPPStatus> {
        let response = await axios.get(MPP_API.status,this.headers)
        return await response.data;
    }

    /**
     * @deprecated this should be removed by passing this argument from the mw
     */
    async getServiceIdFromWorkflow() {
        let headers = {headers : {...this.headers.headers, 'x-sessionid': this.guid, 'x-version': '10.2.0'}}
        let workflow = await axios.get(`${MPP_URL}/api/workflows/configurations`,headers);
        const serviceId = workflow.data.createSessionInfo.serviceId;
        //https://uat.mppglobal.com/api/subscriptions/search?serviceIds=21838
        let service = await axios.get(`${MPP_URL}/api/subscriptions/search?serviceIds=${serviceId}`,headers)
        console.log(service.data.subscriptionsInfo[0]);
        return service.data.subscriptionsInfo[0].resourceReference;

    }

    async checkBIN(card : string) {
        let headers = {headers : {...this.headers.headers, 'x-sessionid': this.guid}}
        let response = await axios.get(MPP_API.binCheck(card),headers)
        return await response.data;
    }

    async addSubscription(
        accountReference: string,
        paymentMethod : string,
        userSubscription: UserSubscription,
        cvv? : string,
        voucher?: string
    ) {
        let data : AddSubscription = {
            cvv : cvv as string,
            voucherCode : voucher as string,
            paymentMethod: paymentMethod,
            enhancedProcessingData : {
                acceptHeader: "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
		        colorDepth: this.getColorDepth(),
		        ip: "2a01:c50e:9e80:400:6ce1:423d:5133:fb4d",
		        javaEnabled: navigator.javaEnabled(),
		        javascriptEnabled: true,
		        language: window.navigator.language,
		        screenHeight: window.innerHeight,
		        screenWidth: window.innerWidth,
		        challengeWindowSize: "WINDOWED_600X400",
		        timezone:  new Date().getTimezoneOffset() / 60,
		        userAgent: window.navigator.userAgent
            },
            pricing : {
                subscriptionPriceReference : userSubscription.pricingInfo[0].resourceReference //'0010M6L5SC15S0Z973'
            }
        }
        let headers = {headers : {...this.headers.headers, 'x-sessionid': this.guid}}
        let response = await axios.post<AddSubsctiptionResponse>(MPP_API.addSubscription(accountReference),data,headers)
        return response.data;
    }

    async addOflineSubscription(
        accountReference: string,
        paymentMethod : string,
        userSubscription: UserSubscription,
        asynchronousInitiationParameters : AsynchronousInitiationParameters,
        voucher?: string
    ) {
        let data : AddSubscription = {
            voucherCode: voucher as string,
            paymentMethod,
            asynchronousInitiationParameters,
            pricing: {
                subscriptionPriceReference: userSubscription.pricingInfo[0].resourceReference //'0010M6L5SC15S0Z973'
            }
        }
        let headers = {headers : {...this.headers.headers, 'x-sessionid': this.guid}}
        let response = await axios.post<AddSubsctiptionResponse>(MPP_API.addSubscription(accountReference),data,headers)
        return response.data;
    }

    async patchSubscription(accountReference: string, data : AddSubscription) {
        let headers = {headers : {...this.headers.headers, 'x-sessionid': this.guid, 'x-version' : '10.9.0'}}
        data.pricing = { subscriptionPriceReference : '0010M6L5SC15S0Z973'}
        let response = await axios.patch<AddSubsctiptionResponse>(MPP_API.addSubscription(accountReference),data,headers)
        return response.data;
    }

    async addCard(accountReference : string, data : PaymentDetailsCard) {
        let headers = {headers : {...this.headers.headers, 'x-sessionid': this.guid}}
        let response = await axios.post<AddSubsctiptionResponse>(MPP_API.addCard(accountReference),data,headers)
        return response.data;
    }

    async deleteCard(accountReference : string, paymentDetailReference : string) {
        let headers = {headers : {...this.headers.headers, 'x-sessionid': this.guid}}
        let response = await axios.delete<AddSubsctiptionResponse>(MPP_API.deleteCard(accountReference,paymentDetailReference),headers)
        return response.data;
    }

    async handlePay(accountReference: string, subscription: UserSubscription ,paymentMethod: string, cvc?: string, voucherCode? : string) {
        let response = this.addSubscription(accountReference,paymentMethod,subscription,cvc,voucherCode);
        let data = await response;
        let state : ThreeDSecureParams;
        if (data?.asynchronousProcessingParameters?.threeDSecureVersion === 'V1') {
            state = {
                ThreeDSecureAcsUrl : data!.asynchronousProcessingParameters!.threeDSecureAcsUrl,
                MD : data!.asynchronousProcessingParameters!.threeDSecureMD,
                PaReq : data!.asynchronousProcessingParameters!.threeDSecurePaReq
            }
        } else if (data?.asynchronousProcessingParameters?.threeDSecureVersion === 'V2'){
            state = {
                threeDSMethodData : data!.asynchronousProcessingParameters!.threeDSecureEncodedMethodData,
                ThreeDSecureAcsUrl : data!.asynchronousProcessingParameters!.threeDSecureAcsUrl
            }
        } else {
            state = {
                addSubscriptionResult : data
            }
        }
        return state;
    }

    async validateVoucher(voucher : string) {
        let headers = {headers : {...this.headers.headers, 'x-sessionid': this.guid}}
        let response;
        try {
            response = await axios.get<VoucherResponseInfo>(MPP_API.voucher(voucher),headers)
            return response.data
        } catch (e) {
            console.log(e.response);
            throw e.response.data;
        }
    }

    private getColorDepth() : string {
        const colorDepth = window.screen.colorDepth;
        const ret = Object.entries(MPPService.colorDepthValues).find(([key,value]) => {
            let [min,max] = key.split('-').map(x => parseInt(x));
            return colorDepth >= min && colorDepth <= max;
        })
        return ret ? ret[1] : 'TWENTY_FOUR_BITS';
    }

}

export default new MPPService()
