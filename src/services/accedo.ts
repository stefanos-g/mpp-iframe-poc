import axios from 'axios';
import {ACCEDO_URL} from "../constants";



class AccedoService {
    async getVersion() {
        return axios.get(`${ACCEDO_URL}/diagnostics/version?platform=web`);
    }
}

export default new AccedoService();
