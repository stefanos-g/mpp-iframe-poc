import {useLocation} from "react-router";

function Error() {
    const location = useLocation()

    return(
        <div>
            {JSON.stringify(location.state)}
        </div>
    )
}

export default Error
