import React, {useCallback, useEffect, Suspense, useState} from 'react';
import {
    useAuthState,
    useAuthDispatch,
    loginUser,
    retrieveUser,
    paymentDetails as getPaymentDetails,
    getSubscription
} from '../context';
import {useLocation} from "react-router";
import {Grid, Snackbar} from "@material-ui/core";
import { CreditCard } from "../components/paymentMethod/creditCard";
import {NewCreditCard} from "../components/paymentMethod/newCreditCard";
import mppService from '../services/mpp';
import {Voucher} from "../components/paymentMethod/voucher";
import {PayPalCard} from "../components/paymentMethod/paypalCard";
import {AppBar} from "../components/AppBar";
import {Footer} from "../components/Footer";
import {CustomOfflineList} from "../components/paymentMethod/customOffline/CustomOfflineList";
import {useNotifications} from "../context/context";
import {Alert, AlertTitle, Color} from "@material-ui/lab";


function Login() {
    const dispatch = useAuthDispatch() as any;
    const { user, paymentDetails } = useAuthState();
    let location = useLocation();
    let searchParams = new URLSearchParams(location.hash.replace(/#!\/.*?\?/,''));
    const guid = searchParams.get('guid') as string
    mppService.guid = guid;
    let serviceId = searchParams.get('subscription') as string //|| '0010C06EFWZ0MJM7N0'// || '0010CELKQGY21XOYQ0'
    const { notification } = useNotifications();
    const [notificationOpen, setNotificationOpen] = useState(false);

    const login = useCallback(async() => {
        try {
            let response = await loginUser(dispatch,guid);
            if (!response!.accountReference) return;
            else {
                const user = retrieveUser(dispatch,guid, response!.accountReference)
                const detail = getPaymentDetails(dispatch, guid, response!.accountReference)
                await Promise.all([user,detail])
            }
        } catch (error) {
            console.log(error);
        }
    },[guid]);

    const loadSubscriptions = useCallback(async() => {
        serviceId = serviceId ? serviceId: await mppService.getServiceIdFromWorkflow();
        return await getSubscription(dispatch,guid,serviceId);
    },[guid,serviceId])

    useEffect(() => {login()},[login])
    useEffect(() => {loadSubscriptions()},[loadSubscriptions])
    useEffect(() => {
        notification?.message && setNotificationOpen(true);
    }, [notification])

    return (
       <Suspense fallback={<div>Loading... </div>}>
           {/*<AppBar/>*/}
           <Grid
               container
               spacing={1}
               direction="row"
               justify="flex-start"
               alignItems="flex-start"
           >
               <Grid item xs={12} sm={12} md={12}>
                    <NewCreditCard/>
               </Grid>
               <Grid item xs={12} sm={4} md={4}>
                   <Voucher/>
               </Grid>
               { paymentDetails && paymentDetails!.cards!.map((card) => <Grid item xs={12} sm={4} md={4} key={card.cardNumber}><CreditCard card={card}/></Grid> )}
               <Grid item xs={12} sm={4} md={4}>
                   <PayPalCard/>
               </Grid>
               <CustomOfflineList />
           </Grid>
           <Snackbar
               anchorOrigin={{
                   vertical: 'top',
                   horizontal: 'center'
               }}
               open={notificationOpen}
               onClose={() => setNotificationOpen(false)}
               autoHideDuration={6000}
               message={notification?.message}
           >
               <Alert severity={notification!.type as Color} variant="filled" onClose={() => setNotificationOpen(false)}>
                   <AlertTitle style={{textTransform:'capitalize'}}>{notification?.type}</AlertTitle>
                   {notification?.message}
               </Alert>
           </Snackbar>
           {/*<Footer/>*/}
       </Suspense>

    );
}

export default Login;
