import {
    useAuthState,
} from '../context';
import {useLocation} from "react-router";
import {AddSubsctiptionResponse} from "../domain/model";
import {Button, Card, CardActions, CardContent, CardHeader, Container, Typography} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowRight} from "@fortawesome/free-solid-svg-icons";


function Summary() {
    const { user } = useAuthState();
    const {t} = useTranslation();
    let location = useLocation();

    const reference = location.state as AddSubsctiptionResponse;

    const handleComplete = () => {
        window.top.postMessage({eventID: 'journeyCompleted'}, '*');
    }

    return (
        <Container>
            <Card>
                <CardHeader>
                    <Typography component='h2'>
                        {t('labelOrdersummary')}
                    </Typography>
                </CardHeader>
                <CardContent>
                    <Typography>
                        {t('orderReference')} {reference.orderReference}
                    </Typography>
                    <Typography>
                        {t('totalPrice')}: {reference.amountCharged} - {reference.currency}
                    </Typography>
                    <Typography>
                        {t('orderDescLine1')} {user!.email}
                    </Typography>
                    <Typography>
                        {t('orderDescLine2')}
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button
                        startIcon={<FontAwesomeIcon icon={faArrowRight}/>}
                        onClick={handleComplete}
                    >
                        {t('orderButton')}
                    </Button>
                </CardActions>
            </Card>
        </Container>
    );
}

export default Summary;
