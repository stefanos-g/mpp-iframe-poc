import {useHistory, useLocation} from "react-router";
import {useEffect} from "react";
import mppService from "../services/mpp";
import {useAuthState} from "../context";
import {AddSubsctiptionResponse, AsynchronousProcessingParameters} from "../domain/model";

export type CustomOfflineState = {
    redirectUrl? : string
    embedded? : boolean
}

export const CustomOfflinePayment = () => {
    const location = useLocation()
    const history = useHistory()
    const customOfflineState = location.state as CustomOfflineState
    const {user} = useAuthState()
    let searchParams = new URLSearchParams(location.search);
    const async = searchParams.get('async') as string

    const storageListener = async (event : StorageEvent) => {
        console.log(event, location)
        window.removeEventListener('storage',storageListener);
        if( event.newValue === 'true') {
            const response = await mppService.patchSubscription(user!.accountReference,{
                paymentMethod:'fortumo',
                asynchronousProcessingParameters: {
                    Stage: 'COMPLETE'
                } as AsynchronousProcessingParameters
            }) as AddSubsctiptionResponse
            history.push('/summary',response)
        } else {
            history.push('/error')
        }
    }

    useEffect(() => {
        if ( async ) return;
        console.log("Listening for storage events", location);
        window.addEventListener('storage',storageListener)
        return () => {
            window.removeEventListener('storage',storageListener)
        }
    })

    useEffect(() => {
        if(customOfflineState && !customOfflineState?.embedded) {
            const win = window.open(customOfflineState.redirectUrl, "_blank");
            win?.focus();
        }
    })

    if (async) {
        localStorage.setItem("async",async)
        localStorage.removeItem("async")
        return <div>Please close this window and return to the hayu app</div>
    }

    return (
        <>
            { customOfflineState?.embedded && <iframe
                title='Custom Payment iFrame'
                style={{width: '100%', height:'100%', borderWidth:'0px', minHeight:'600px', overflow:'hidden'}}
                src={customOfflineState.redirectUrl}
            /> }
            { !customOfflineState?.embedded && <div>
                Please finish the transaction in the other window
            </div>}
        </>
    )
}
