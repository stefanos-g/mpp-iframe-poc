import React, {useEffect, useState} from 'react';
import {
    useAuthState,
} from '../context';
import {useHistory, useLocation} from "react-router";
import {AddSubscription, AddSubsctiptionResponse, ThreeDSecureParams} from "../domain/model";
import {Container} from "@material-ui/core";
import mppService from '../services/mpp';
import {MPP_3DSV1} from "../constants";


function Login() {
    const { user } = useAuthState();
    let location = useLocation();
    const history = useHistory();
    const params = location.state as ThreeDSecureParams;

    const handleThreeDSecure = async (event : any) => {
        const data = event.data as ThreeDSecureParams;
        const body : AddSubscription = {
            paymentMethod: 'CreditCard',
            asynchronousProcessingParameters : {
                MD : data.MD,
                PaRes: data.PaRes
            }
        }
        const response = await mppService.patchSubscription(user!.accountReference,body) as AddSubsctiptionResponse
        if (response.asynchronousProcessingParameters?.threeDSecureStatus === "ChallengeRequired"){
            const state : ThreeDSecureParams = {
                ThreeDSecureAcsUrl : response.asynchronousProcessingParameters.threeDSecureChallengeRequestUrl,
                creq: response.asynchronousProcessingParameters.threeDSecureEncodedCreq
            }
            history.push('/threeDsecure',state);
        } else {
            window.top.postMessage({eventID: 'paymentSuccess', order: response.orderReference}, '*');
            history.push('/summary',response)
        }
    };

    useEffect(() => {
        const form = document.getElementById('threeDSecureForm') as HTMLFormElement;
        form.action = params.MD ? MPP_3DSV1 : decodeURIComponent(params!.ThreeDSecureAcsUrl as string);
        params.ThreeDSecureAcsUrl = undefined;
        form.submit();
    })

    useEffect(() => {
        window.addEventListener('message',handleThreeDSecure);
        return () => {
            window.removeEventListener('message',handleThreeDSecure);
        }
    })

    return (
        <Container>
            <iframe
                title='threeDSecureFrame'
                id="threeDSecureFrame"
                name="threeDSecureFrame"
                className="center-block"
                style={{width: '100%', height:'400px', border: 'none'}}
            />

            <form id="threeDSecureForm" name="threeDSecureForm" method="post" target="threeDSecureFrame">
                {
                    Object.entries(params).map(([key,value]) => (
                        <input key={key} type="hidden" name={key} value={value}/>
                    ))
                }
            </form>
        </Container>
    );
}

export default Login;
