import React, {useCallback, useEffect, Suspense} from 'react';
import {
    useAuthState,
    useAuthDispatch,
    loginUser,
    retrieveUser,
    paymentDetails as getPaymentDetails,
    getSubscription
} from '../context';
import {useLocation} from "react-router";
import mppService from '../services/mpp';
import Cards from "react-credit-cards";
import {useTranslation} from "react-i18next";


function DetailViewer() {
    const dispatch = useAuthDispatch() as any;
    const { user, paymentDetails } = useAuthState();
    const { t } = useTranslation()
    let location = useLocation();
    let searchParams = new URLSearchParams(location.hash.replace(/#!\/.*?\?/,''));
    const guid = searchParams.get('guid') as string
    mppService.guid = guid;
    const serviceId = searchParams.get('subscription') as string || '0010CELKQGY21XOYQ0'

    const login = useCallback(async() => {
        try {
            let response = await loginUser(dispatch,guid);
            if (!response!.accountReference) return;
            else {
                const user = retrieveUser(dispatch,guid, response!.accountReference)
                const detail = getPaymentDetails(dispatch, guid, response!.accountReference)
                await Promise.all([user,detail])
            }
        } catch (error) {
            console.log(error);
        }
    },[guid]);

    const loadSubscriptions = useCallback(async() => {
        return await getSubscription(dispatch,guid,serviceId);
    },[guid,serviceId])

    useEffect(() => {login()},[login])
    useEffect(() => {loadSubscriptions()},[loadSubscriptions])


    return (
        <Suspense fallback={<div>Loading... </div>}>
            <div className="preview">
            { paymentDetails && paymentDetails!.cards!.map((card) => <Cards
                cvc=''
                expiry={card.expiryDate}
                name={card.associatedName}
                number={
                    card.cardNumber
                        .replace(/-/g,'')
                        .replace(/\*/g,'•')
                }
                preview={true}
                issuer={card.cardType}
                locale={{valid : t('expiresCardLabel')}}
            /> )}
            </div>
        </Suspense>
    );
}

export default DetailViewer;
