import {Avatar, Box, Chip} from "@material-ui/core";
import {useCallback, useEffect, useState} from "react";
import accedoService from '../services/accedo'
import mppService, {MPPStatus} from '../services/mpp'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faServer, faCheck, faCross, faGlobe } from '@fortawesome/free-solid-svg-icons';

export const Footer = () => {
    const [accedoVersion, setAccedoVersion] = useState('');
    const [mppStatus, setMppStatus] = useState({} as MPPStatus);

    const getVersion = useCallback(async () => {
        let response = await accedoService.getVersion();
        console.log(response.data)
        setAccedoVersion(response.data.version);
    },[]);

    const getMPPStatus = useCallback(async () => {
        let response = mppService.getStatus();
        setMppStatus(await response);
    },[])

    useEffect(() => {
        getVersion()
        getMPPStatus()
    },[getVersion,getMPPStatus])

    return (
        <Box style={{top: '0px', position : 'absolute', right: '0px'}}>
            <Chip
                label={accedoVersion}
                avatar={<Avatar src="https://id.one.accedo.tv/login/favicons/favicon-32x32.png"/>}
            />
            <Chip
                label={mppStatus.buildVersion}
                avatar={<Avatar src="https://support.mppglobal.com/wp-content/themes/mpp-support/favicon.png"/>}
            />
            <Chip
                label={<FontAwesomeIcon icon={ mppStatus.databaseAvailable ? faCheck : faCross }/>}
                icon={<FontAwesomeIcon icon={faServer} />}
            />
            <Chip
                label={<FontAwesomeIcon icon={ mppStatus.endpointAvailable ? faCheck : faCross }/>}
                icon={<FontAwesomeIcon icon={faGlobe} />}
            />
        </Box>
    )
}
