import {AppBar as ReactAppBar, Badge, Chip, Toolbar, Typography} from "@material-ui/core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStarOfLife } from "@fortawesome/free-solid-svg-icons";
import {useAuthState} from "../context";

export const AppBar = () => {
    const { subscription, voucher } = useAuthState();

    const formatPrice = (price? : number, currency? : string) => {
        if(!price || !currency) return "0.0";
        return new Intl.NumberFormat(navigator.language, { style: 'currency', currency }).format(price ? price: 0)
    }
    return (
        <ReactAppBar position="static" color="default">
            <Toolbar>
                <Typography>
                    Price :
                </Typography>
                <Badge
                    invisible={voucher === undefined}
                    color="secondary"
                    badgeContent={'-'+voucher?.voucherInfo?.offerInfo?.lowStart?.percentage+ '%'}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'right',
                    }}
                >
                    <Chip label={formatPrice(subscription?.pricingInfo[0].renewalPrice,subscription?.pricingInfo[0].currency)}/>
                </Badge>
            </Toolbar>
        </ReactAppBar>
    );
}
