import {
    Button,
    Card,
    CardActions,
    CardContent,
    CardMedia,
    Typography,
    Collapse,
    TextField, Grid
} from "@material-ui/core";
import Cards, {Focused} from "react-credit-cards";
import React, {useState} from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCreditCard } from "@fortawesome/free-solid-svg-icons";
import './creditCard.scss'
import {useTranslation} from "react-i18next";
import mppService from '../../services/mpp';
import {useAuthState} from "../../context";
import {PaymentDetailsCard, UserSubscription} from "../../domain/model";
import {useHistory} from "react-router";
import {useNotifications} from "../../context/context";

export const NewCreditCard = () => {
    const { t } = useTranslation();
    const {user, voucherCode, subscription} = useAuthState();
    const {sendNotification}= useNotifications();
    const history = useHistory();

    const [creditState, setCreditState] = useState({
        cvc: '',
        expiry: '',
        focus: '',
        name: '',
        number: '',
        prevNumber: '',
        valid: false,
        type: ''
    });

    const handleInputFocus = (e : any) => {
        setCreditState({...creditState, focus: e.target.name });
    }

    const handleDate = (date : string) => {
        return date.replace(
            /^([1-9]\/|[2-9])$/g, '0$1/' // 3 > 03/
        ).replace(
            /^(0[1-9]|1[0-2])$/g, '$1/' // 11 > 11/
        ).replace(
            /^([0-1])([3-9])$/g, '0$1/$2' // 13 > 01/3
        ).replace(
            /^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2' // 141 > 01/41
        ).replace(
            /^([0]+)\/|[0]+$/g, '0' // 0/ > 0 and 00 > 0
        ).replace(
            /[^\d\/]|^[\/]*$/g, '' // To allow only digits and `/`
        ).replace(
            /\/\//g, '/' // Prevent entering more than 1 `/`
        );
    }

    const handleInputChange = (e : any) => {
        let { name, value } = e.target;
        value = name === 'expiry' ? handleDate(value): value;
        setCreditState({...creditState, [name]: value });
    }

    const addCard = async () => {
        const data : PaymentDetailsCard = {
            setDefault: true,
            skipPreAuth: true,
            cvv: creditState.cvc,
            cardNumber: creditState.number.replaceAll(' ',''),
            cardType: creditState.type,
            associatedName: creditState.name,
            expiryDate: creditState.expiry
        }
        try {
            await mppService.addCard(user!.accountReference, data)
            const state = await mppService.handlePay(user!.accountReference, subscription as UserSubscription, 'CreditCard', creditState.cvc, voucherCode);
            if (state.addSubscriptionResult) {
                window.top.postMessage({
                    eventID: 'paymentSuccess',
                    order: state.addSubscriptionResult.orderReference
                }, '*');
                history.push('/summary', state.addSubscriptionResult)
            } else {
                history.push('/threeDsecure', state)
            }
        } catch(e) {
            sendNotification!({message: e.response.data.errorMessage, type:'error'})
        }
    }

    const handleCardCallback = async (type: any, valid: any) => {
        if ( creditState.number.replaceAll(' ','').substr(0,6) !== creditState.prevNumber.substr(0,6) && creditState.number.length > 5) {
            try {
                let status = await mppService.checkBIN(creditState.number.replaceAll(' ','').substr(0,6)) as any;
                setCreditState({...creditState, valid: valid, type: status.cardScheme,prevNumber: creditState.number});
            } catch(error) {
                sendNotification!({message: error.response.data.errorMessage, type:'error'})
            }
        } else {
            setCreditState({...creditState, valid: valid, prevNumber: creditState.number});
        }
    }

    return (
        <Card style={{ minWidth: '275px'}} >
            {/*<CardContent>*/}
            {/*    <Typography color="textPrimary" variant='h5' gutterBottom>*/}
            {/*        {t('AddCard')}*/}
            {/*    </Typography>*/}
            {/*</CardContent>*/}
            <Grid container
                  spacing={4}
                  direction="row"
                  justify="flex-start"
                  alignItems="flex-start">
                <Grid item xs={12} sm={4} md={4}>
                    <CardMedia style={{marginTop:'20px'}}>
                        <Cards
                            cvc={creditState.cvc}
                            expiry={creditState.expiry}
                            name={creditState.name}
                            focused={creditState.focus as Focused}
                            number={creditState.number}
                            preview={false}
                            placeholders={{
                                name : t('labelCardHolderName')
                            }}
                            callback={handleCardCallback}
                            locale={{valid : t('expiresCardLabel')}}
                        />
                    </CardMedia>
                    <Button
                        variant="contained"
                        color="secondary"
                        style={{marginTop:'10px', marginLeft:'10px'}}
                        onClick={addCard}
                        startIcon={<FontAwesomeIcon icon={faCreditCard}/>}>
                        {t('paymentViewSubmitBtnText')}
                    </Button>
                </Grid>
                <Grid item xs={12} sm={8} md={8}>
                    <CardMedia>
                        <form>
                            <Grid container
                                  spacing={1}
                                  >
                                <Grid item xs={12} sm={12} md={12}>
                            <TextField
                                fullWidth
                                type={'tel'}
                                name={'number'}
                                placeholder={t('textCardNumber')}
                                onChange={handleInputChange}
                                onFocus={handleInputFocus}
                                required={true}
                                label={t('textCardNumber')}
                                helperText={t('errorCardNumber2')}
                            />
                                </Grid>
                                <Grid item xs={12} sm={12} md={12}>
                            <TextField
                                fullWidth
                                type={'text'}
                                name={'name'}
                                placeholder={t('labelCardHolderName')}
                                onChange={handleInputChange}
                                onFocus={handleInputFocus}
                                required={true}
                                label={t('labelCardHolderName')}
                                helperText={t('errorCardHoldersName')}
                            />
                                </Grid>
                                <Grid item xs={12} sm={6} md={6}>
                            <TextField
                                fullWidth
                                type={'tel'}
                                name={'expiry'}
                                placeholder={t('textExpireDate')}
                                onChange={handleInputChange}
                                onFocus={handleInputFocus}
                                required={true}
                                value={creditState.expiry}
                                label={t('textExpireDate')}
                                helperText={t('textExpireDate')}
                            />
                                </Grid>
                                <Grid item xs={12} sm={6} md={6}>
                            <TextField
                                fullWidth
                                type={'tel'}
                                name={'cvc'}
                                placeholder={'CVC'}
                                onChange={handleInputChange}
                                onFocus={handleInputFocus}
                                required={true}
                                label={t('labelCSV')}
                                helperText={t('textExpireDate')}
                            />
                                </Grid>
                            </Grid>
                        </form>
                    </CardMedia>
                </Grid>
            </Grid>
        </Card>
    );
}
