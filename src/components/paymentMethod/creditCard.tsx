import {
    Button,
    Card,
    CardActions,
    CardContent,
    CardMedia,
    Typography,
    TextField, Slide, IconButton
} from "@material-ui/core";
import Cards, {Focused} from "react-credit-cards";
import React, {useState} from "react";
import {ThreeDSecureParams, UserCard, UserSubscription} from "../../domain/model";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faCreditCard, faTrash} from "@fortawesome/free-solid-svg-icons";
import './creditCard.scss'
import {useTranslation} from "react-i18next";
import mppService from "../../services/mpp";
import {useAuthState} from "../../context";
import {useHistory} from 'react-router';
import {useNotifications} from "../../context/context";
import mpp from "../../services/mpp";

type CreditCardProps = {
    card : UserCard
}

export const CreditCard = ({ card }: CreditCardProps) => {
    const { t } = useTranslation();
    const [showCvc,setShowCvc] = useState(false);
    const [cvc, setCvc] = useState('');
    const [focus,setFocus] = useState('');
    const { user, voucherCode, subscription } = useAuthState();
    const history = useHistory();
    const {sendNotification} = useNotifications();

    const handlePay = async () => {
        try {
            let state = await mppService.handlePay(user!.accountReference, subscription as UserSubscription,'CreditCard',cvc,voucherCode);
            if (state.addSubscriptionResult) {
                window.top.postMessage({eventID: 'paymentSuccess', order: state.addSubscriptionResult.orderReference}, '*');
                history.push('/summary',state.addSubscriptionResult)
            } else {
                history.push('/threeDsecure',state)
            }
        } catch (e) {
            sendNotification!({message: e.response.data.errorMessage, type:'error'})
        }
    }

    // const handleDelete = async() => {
    //     let response = await mppService.deleteCard(user!.accountReference,card.pan);
    //     console.log(response);
    //     sendNotification!({message: 'DetailID '+ card.paymentDetailReference, type:'info'})
    // }

    const handleMouseEnter = () => {
        setFocus('cvc')
        setShowCvc(true);
    }
    const handleMuseLeave = () => {
        setFocus('')
        setShowCvc(false);
    }

    return (
        <Card>
            {/*<CardContent>*/}
            {/*    <Typography color="textPrimary" variant='h5' gutterBottom>*/}
            {/*        {card.cardType}*/}
            {/*    </Typography>*/}
            {/*</CardContent>*/}
            <CardMedia>
                <Cards
                    cvc={cvc}
                    expiry={card.expiryDate}
                    name={card.associatedName}
                    number={
                        card.cardNumber
                            .replace(/-/g,'')
                            .replace(/\*/g,'•')
                    }
                    preview={true}
                    issuer={card.cardType}
                    locale={{valid : t('expiresCardLabel')}}
                    focused={focus as Focused}
                />
            </CardMedia>
            <CardActions onMouseEnter={handleMouseEnter} onMouseLeave={handleMuseLeave}>
                {/*<IconButton color="secondary" onClick={handleDelete}>*/}
                {/*    <FontAwesomeIcon icon={faTrash}/>*/}
                {/*</IconButton>*/}
                <Button onClick={handlePay} startIcon={<FontAwesomeIcon icon={faCreditCard}/>}
                        color="secondary" variant="contained">
                    {/*<Typography>*/}
                        {t('Pay')}
                    {/*</Typography>*/}
                </Button>
                <Slide in={showCvc} direction={'right'}>
                    <TextField
                        size="small"
                        margin="dense"
                        placeholder={'CVC'}
                        onChange={(e) => setCvc(e.target.value)}
                    />
                </Slide>
            </CardActions>
        </Card>
    );
}
