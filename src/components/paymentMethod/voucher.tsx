import {
    Button,
    Card,
    CardActions,
    CardContent,
    TextField,
} from "@material-ui/core";
import React, {useState} from "react";
import './creditCard.scss'
import {Alert} from "@material-ui/lab";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSignInAlt } from "@fortawesome/free-solid-svg-icons";
import {useAuthDispatch, useAuthState} from "../../context";
import {validateVoucher} from "../../context/actions";
import {useTranslation} from "react-i18next";
import {useNotifications} from "../../context/context";

export const Voucher = () => {
    const dispatch = useAuthDispatch();
    const {t} = useTranslation()
    const { user, paymentDetails, guid } = useAuthState();
    const [voucher,setVoucher] = useState('');
    // const [error,setError] = useState('');
    const {sendNotification}  = useNotifications();

    const handleInput = (e : any) => {
        let { value } = e!.target;
        setVoucher(value);
        // setError('')
    }

    const validateVoucherAction = async () => {
        try {
            const result = await validateVoucher(dispatch,guid ? guid : '',voucher);
            sendNotification!({ message: `Voucher applied for ${result?.voucherInfo.offerInfo.lowStart.percentage}% discount`, type: 'success'})
            console.log(JSON.stringify(result?.purchaseInfo))
        } catch (e) {
            console.log(e);
            sendNotification!({ message: e.errorMessage as string, type: 'warning'})
        }

    }

    return (
        <Card>
            {/*<CardContent>*/}
            {/*    <Typography color="textPrimary" variant='h5' gutterBottom>*/}
            {/*        Enter Voucher*/}
            {/*    </Typography>*/}
            {/*</CardContent>*/}
            <CardContent>
                <TextField
                    variant="outlined"
                    fullWidth
                    name='Voucher'
                    placeholder='Voucher Code'
                    type="text"
                    label="Voucher"
                    onChange={handleInput}
                />
            </CardContent>
            <CardActions >
                <Button
                    variant="contained"
                    color="secondary"
                    onClick={validateVoucherAction}
                    startIcon={<FontAwesomeIcon icon={faSignInAlt}/>}
                >
                    {t('voucherApplyBtn')}
                </Button>
            </CardActions>
        </Card>
    );
}
