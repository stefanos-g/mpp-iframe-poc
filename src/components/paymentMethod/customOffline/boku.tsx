import {Button, Card, CardActions, CardMedia, Grid, IconButton} from "@material-ui/core";
import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCreditCard, faExternalLinkAlt} from "@fortawesome/free-solid-svg-icons";
import mppService from "../../../services/mpp";
import {useAuthState} from "../../../context";
import {AsynchronousInitiationParameters, UserSubscription} from "../../../domain/model";
import {useHistory} from "react-router";
import {CustomOfflineState} from "../../../pages/CustomOfflinePayment";
import {paymentMethodMapper} from "./CustomOfflineList";

type FortumoProps = {
    data : string
}

const BokuCard = ({data} : FortumoProps) => {
    const history = useHistory()
    const {user, voucherCode, subscription} = useAuthState()
    const variant = paymentMethodMapper(data) as string[]

    const handlePay = async (embedded : boolean) => {
        let data : AsynchronousInitiationParameters = {
            country: 'PH',
            Stage: "START",
            redirect: "https://www.stg.hayu.com/customPayment?async=true",
            failure_redirect: "https://www.stg.hayu.com/customPayment?async=false"
        }
        const response = await mppService.addOflineSubscription(user!.accountReference,variant[0], subscription as UserSubscription, data, voucherCode)
        if( response.asynchronousProcessingParameters && response.asynchronousProcessingParameters.redirectUrl) {
            history.push('/customPayment', {
                redirectUrl: response.asynchronousProcessingParameters.redirectUrl,
                embedded
            } as CustomOfflineState)
        }
    }

    return (
        <Card style={{minHeight: '150px'}}>
            <CardMedia
                image={`${process.env.PUBLIC_URL}/img/${variant[2].toLowerCase()}_logo.png`}
                style={{ height : '100px', width:'215px', margin: 'auto', backgroundSize:'100px'}}
            />
            <CardActions>
                <Button onClick={() => handlePay(true)} startIcon={<FontAwesomeIcon icon={faCreditCard}/>}
                        color="secondary" variant="contained">
                    {'Pay'}
                </Button>
                <Button onClick={() => handlePay(false)} startIcon={<FontAwesomeIcon icon={faExternalLinkAlt}/>}
                        color="secondary" variant="contained">
                    {'Pay'}
                </Button>
            </CardActions>
        </Card>
    );
}

export default ({ data } : FortumoProps) => (
    <>
        <Grid item xs={6} sm={4} md={4}>
            <BokuCard data={data}/>
        </Grid>
    </>
);
