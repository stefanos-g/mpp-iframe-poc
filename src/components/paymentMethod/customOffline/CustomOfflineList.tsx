import {ComponentType, lazy, useEffect, useState, Suspense} from "react";
import {Grid} from "@material-ui/core";
import Fortumo from './fortumo';
import {useAuthState} from "../../../context";

type PaymentProps = {
    paymentMethods: {[key: string] : ComponentType<any>}
}

export type CustomOfflineListProps = {
    allowedPaymentMethods : string[];
}

export const ALLOWED_PAYMENT_METHODS = [
    'fortumo',
    "BokuMMS",
    "BokuPaymayaMMS",
    "BokuGcashMMS"
]

export const paymentMethodMapper = (method : string) => {
    switch (method) {
        case 'fortumo' : return ['fortumo','fortumo','fortumo']
        case 'BokuMMS' : return ['BokuMMS','boku','boku']
        case 'BokuPaymayaMMS' : return ['BokuPaymayaMMS','boku','paymaya']
        case 'BokuGcashMMS' : return ['BokuGcashMMS','boku','gcash']
        default: return null;
    }
}

const importPaymentMethod = (customPaymentMethod : string) =>
    lazy(() =>
        import(`./${customPaymentMethod}`)
    );

const PaymentMethodList = ({ paymentMethods } : PaymentProps) => {
    return <>
        {Object.entries(paymentMethods).map(([type,PaymentMethod]) => (
            <PaymentMethod key={type} data={type} />
        ))}
    </>
}


export const CustomOfflineList = () => {
    const [paymentMethods, setPaymentMethods] = useState({} as {[key: string] : ComponentType} )
    const { subscription } = useAuthState();
    const addPaymentMethod = (paymentMethod: string, paymentMethodCard : string) => {
        if (paymentMethods[paymentMethod]) return;
        const PaymentMethod = importPaymentMethod(paymentMethodCard);
        setPaymentMethods(methods => ({ ...methods, [paymentMethod]: PaymentMethod }));
    };

    useEffect(() => {
        const allowedPaymentMethods = subscription?.allowedPaymentMethods || [];
        allowedPaymentMethods.map(paymentMethodMapper).filter(it => !!it).forEach(methods => {
            const [method,cardName] = methods as string[];
            addPaymentMethod(method,cardName);
        })
    },[subscription])

    return (
        <Suspense fallback='Loading...'>
            <PaymentMethodList paymentMethods={paymentMethods} />
        </Suspense>
    )
}
