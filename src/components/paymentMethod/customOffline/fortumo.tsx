import {Button, Card, CardActions, CardMedia, Grid, IconButton} from "@material-ui/core";
import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCreditCard, faExternalLinkAlt} from "@fortawesome/free-solid-svg-icons";
import mppService from "../../../services/mpp";
import {useAuthState} from "../../../context";
import {AsynchronousInitiationParameters, UserSubscription} from "../../../domain/model";
import {useHistory} from "react-router";
import {CustomOfflineState} from "../../../pages/CustomOfflinePayment";

type FortumoProps = {
    channelCode : string
    carrier: string
}

const FortumoCard = ({channelCode,carrier} : FortumoProps) => {
    const history = useHistory()
    const {user, voucherCode, subscription} = useAuthState()

    const handlePay = async (embedded : boolean) => {
        let data : AsynchronousInitiationParameters = {
            channel_code: channelCode,
            country_code: 'PH',
            Stage: "START",
            redirect: "https://www.stg.hayu.com/customPayment?async=true",
            failure_redirect: "https://www.stg.hayu.com/customPayment?async=false"
        }
        const response = await mppService.addOflineSubscription(user!.accountReference,'fortumo', subscription as UserSubscription, data, voucherCode)
        if( response.asynchronousProcessingParameters && response.asynchronousProcessingParameters.redirectUrl) {
            history.push('/customPayment', {
                redirectUrl: response.asynchronousProcessingParameters.redirectUrl,
                embedded
            } as CustomOfflineState)
        }
    }

    return (
      <Card style={{minHeight: '150px'}}>
          <CardMedia
              image={`${process.env.PUBLIC_URL}/img/${carrier}_logo.png`}
              style={{ height : '100px', width:'215px', margin: 'auto', backgroundSize:'100px'}}
          />
          <CardActions>
              <Button onClick={() => handlePay(true)} startIcon={<FontAwesomeIcon icon={faCreditCard}/>}
                      color="secondary" variant="contained">
                  {'Pay'}
              </Button>
              <Button onClick={() => handlePay(false)} startIcon={<FontAwesomeIcon icon={faExternalLinkAlt}/>}
                      color="secondary" variant="contained">
                  {'Pay'}
              </Button>
          </CardActions>
      </Card>
    );
}

export default ({ data } : any) => (
    <>
        <Grid item xs={6} sm={4} md={4}>
            <FortumoCard channelCode={'sandbox-ph'} carrier={'smart'}/>
        </Grid>
        <Grid item xs={6} sm={4} md={4}>
            <FortumoCard channelCode={'sandbox-ph'} carrier={'globe'}/>
        </Grid>
    </>
);
