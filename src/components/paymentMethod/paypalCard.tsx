import {
    Button,
    Card,
    CardActions,
    CardMedia,
} from "@material-ui/core";
import React, {useEffect, useState} from "react";

import mppService from "../../services/mpp";
import {useAuthState} from "../../context";
import {useHistory} from "react-router";
import {UserSubscription} from "../../domain/model";

declare const paypal : any;

export const PayPalCard = () => {
    const { user, voucherCode, subscription } = useAuthState();
    const history = useHistory();
    const [loaded,setLoaded] = useState(false)

    useEffect(() => {
        const id = 'checkout';
        if (document.getElementById(id) || window.hasOwnProperty('PAYPAL')) {
            setLoaded(true);
            return;
        }
        const scriptTag = document.createElement('script');
        scriptTag.id='checkout';
        scriptTag.src = 'https://www.paypalobjects.com/api/checkout.js';
        scriptTag.addEventListener('load', () => setLoaded(true))
        document.body.appendChild(scriptTag);
    }, [])

    useEffect(() => {
        const accountReference = user?.accountReference;
        if(!loaded || !accountReference || !subscription) return;
        const node = document.getElementById("paypal-button");
        if(node?.innerHTML) node.innerHTML = '';
        paypal.Button.render({
            env: 'sandbox',
            style: {
                size: 'medium',
                color: 'blue',
                shape: 'rect',
                label: 'pay',
                tagline: false
            },
            commit: true,
            payment: async () => {
                console.log(accountReference,subscription);
                const details = await mppService.addSubscription(accountReference,'paypal',subscription as UserSubscription,undefined,voucherCode);
                if (details?.asynchronousProcessingParameters) {
                    const paypalLocation = new URL(details!.asynchronousProcessingParameters!.payPalLocation)
                    console.log(paypalLocation.searchParams.get('token'))
                    return paypalLocation.searchParams.get('token');
                } else {
                    window.top.postMessage({eventID: 'paymentSuccess', order: details.orderReference}, '*');
                    history.push('/summary',details)
                    return null;
                }
            },
            onAuthorize: async (data : any, actions: any) =>{
                const response = await mppService.patchSubscription(accountReference,{paymentMethod:'paypal'})
                console.log(response)
                window.top.postMessage({eventID: 'paymentSuccess', order: response.orderReference}, '*');
                history.push('/summary',response)
            },
            onError: (err: any) => {
                // $rootScope.$broadcast(APP_EVENTS.paymentDetails.paypalComplete);
                // $rootScope.$apply();
            }
        }, '#paypal-button');
    },[loaded,user,subscription])

    return (
        <Card>
            <CardMedia
                image={process.env.PUBLIC_URL + "/img/PayPal.svg"}
                style={{ height : '150px', width:'215px', backgroundSize:'215px 215px', margin: 'auto'}}
            >
            </CardMedia>
            <CardActions>
                <div id="paypal-button" style={{margin:'auto'}}/>
            </CardActions>
        </Card>
    );
}
