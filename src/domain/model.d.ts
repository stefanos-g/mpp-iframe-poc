export interface UserSession {
    accountReference: string
    shortSessionActive: boolean
    shortSessionExpiry: string
    longSessionActive: boolean
    longSessionExpiry: string
}

export interface UserAddressInfo {
    addressType?: string
    houseName?: string
    houseNumber?: string
    street?: string
    townCity?: string
    district?: string
    county?: string
    postCode?: string
    country?: string
    isDefault?: boolean
}

export interface UserInfo {
    accountId: string
    accountReference: string
    status: string
    verified: boolean
    email: string
    clientUserId: string
    salutation: string
    firstName: string
    lastName: string
    dateOfBirth: string
    gender: string
    addresses : UserAddressInfo[]
    customParameters: any
    sessionToken: string
    marketingAllowed: boolean
}

export interface UserCard {
    associatedName: string
    cardNumber: string
    cardType: string
    expiryDate: string
    isDefault: true,
    issueCode: string
    paymentDetailId: string
    authorised: boolean
    bin: string
    pan: string
    paymentDetailReference: string
    creationDate: string
    cardCategory: string
    issuer: string
    cardPurpose: string
    issuingCountry: string
}

export interface UserPaymentDetails {
    cards? : UserCard[]
    payPal : any
}

export interface UserSubscription {
    contractId: number
    contractReference: string
    subscriptionGroup: string
    subscriptionGroupTag: string
    subscriptionGroupStatus: string
    subscriptionDescription: string
    subscriptionTitle: string
    subscriptionType: string
    subscriptionStatus: string
    initialPricingEnabled: boolean
    trialInfo : TrialInfo
    contractInfo : ContractInfo
    pricingInfo: PricingInfo[]
    allowedPaymentMethods : string[]
    customParameters: CustomParameter[]
    cancellationDetails : CancellationDetails
    images: any[]
    groupLicensing: GroupLicensing
    paymentOptions : PaymentOptions
    popularity: number

}

export interface TrialInfo {
    trialEnabled: boolean
    trialUnit: string
    trialPeriod: number
    discountPercentage: number
    nonPaymentTrial: boolean
    trialChargedAsSinglePeriod: boolean
}


export interface ContractInfo {
    contractUnit: string
    contractPeriod: number
    autoRenewDefault: string
}

export interface PricingInfo {
    currency: string
    initialPrice: number
    priceIsGross: boolean
    renewalPrice: number
    taxCategory: string
    resourceReference: string
    paymentMethod: string
    role: string
}

export interface CustomParameter {
    parameterName : string
    parameterType : string
    parameterValue : string
}

export interface CancellationDetails {
    cancellationOption: string
    minimumRefundThresholdEnabled: boolean
    minimumRefundThresholds: any[]
}

export interface GroupLicensing {
    standard: number
    overflow: number
}

export interface PaymentOptions {
    cardTypes : CardType[]
    directDebitTypes: any[]
    bankTransferTypes: any[]
}

export interface CardType {
    name: string
    allowRepeats: boolean
    use3DSecureForAllTransactions: boolean
    use3DSecureOnFirstTransaction: boolean
}

export interface EnhancedProcessingData {
   acceptHeader: string
   colorDepth: string
   ip: string
   javaEnabled: boolean
   javascriptEnabled: boolean
   language: string
   screenHeight: number
   screenWidth: number
   challengeWindowSize: string
   timezone: number,
   userAgent: string
}

export interface SubscriptionPricing {
    subscriptionPriceReference: string
}

export interface AddSubscription {
    voucherCode?: string
    paymentMethod: string,
    OverrideOrderDescription?: string
    cvv?: string
    enhancedProcessingData? : EnhancedProcessingData
    asynchronousProcessingParameters? : ThreeDSecureParams | AsynchronousProcessingParameters
    asynchronousInitiationParameters? : AsynchronousInitiationParameters;
    pricing? : SubscriptionPricing
}

export interface AsynchronousProcessingParameters {
    Stage: 'COMPLETE'
}

export interface AsynchronousInitiationParameters {
    Stage: "START" | "COMPLETE"
    country_code?: string
    channel_code?: string
    country? : string
    redirect?: string
    failure_redirect?: string
}

export interface AsynchronousProcessingParameters {
    threeDSecureAcsUrl: string
    threeDSecureEnrolled?: string
    threeDSecureHtml: string
    threeDSecureMD: string
    threeDSecurePaReq: string
    threeDSecureStatus: string
    threeDSecureVersion: "V1" | "V2"
    threeDSecureId: string;
    threeDSecureDSMethodNotificationUrl: string;
    threeDSecureEncodedMethodData: string;
    threeDSecureChallengeRequestUrl: string;
    threeDSecureEncodedCreq: string;
    payPalLocation: string;
    eSuiteInternalReference : string;
    redirectUrl: string;

}

export interface AddSubsctiptionResponse {
    amountCharged : number
    asynchronousProcessingParameters?: AsynchronousProcessingParameters
    contractReference: string
    currency: string
    paymentType: string
    renewalDate: string
    renewalDay: number
    renewalDayOffset: number
    sessionToken: string
    startDate: string
    orderReference? : string
    subscriptionPriceReference: string
    subscriptionReference?: string
    resourceReference?: string
    subscriptionStatus: string
    trialInfo : TrialInfo
}

export interface ThreeDSecureParams {
    ThreeDSecureAcsUrl?: string
    PaReq? : string
    PaRes?: string
    MD? : string
    threeDSMethodData? : string
    creq? : string
    addSubscriptionResult? : AddSubsctiptionResponse
}

export interface PaymentDetailsCard {
    associatedName: string;
    cardType: string;
    cardNumber: string;
    expiryDate: string;
    cvv: string;
    setDefault: boolean;
    skipPreAuth: boolean;
}

export interface PurchaseInfo {
    purchasePrice: number;
    discountPrice: number;
    renewalPrice: number;
}

export interface ApplicationData {
    name: string;
    message: string;
    message2: string;
}

export interface LowStart {
    percentage: number;
    numberOfPeriods: number;
    lockInPeriods: number;
    closeSubOnExpiry: boolean;
    serviceReferences: string[];
    paymentDetailsRequired: boolean;
}

export interface OfferInfo {
    name: string;
    description: string;
    usageType: number;
    applicationData: ApplicationData;
    lowStart: LowStart;
}

export interface VoucherInfo {
    startDate: Date;
    expiryDate: Date;
    offerInfo: OfferInfo;
}

export interface VoucherResponseInfo {
    purchaseInfo: PurchaseInfo;
    voucherInfo: VoucherInfo;
}
