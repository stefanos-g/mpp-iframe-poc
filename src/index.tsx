import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import Cookies from 'js-cookie';

import i18n from './i18n';

i18n.init({
    fallbackLng: 'en',
    debug: false,
    react: {
        useSuspense: false //   <---- this will do the magic
    },
    backend: {
        customHeaders: {
            // 'x-portability' : '{"locale":"fr-FR"}'
            // 'x-portability' : `${Cookies.get('hayu-portability-local')}`
        },
        loadPath: 'https://api.stg.hayu.com/app/mppiframe/config',
        queryStringParams: { platform: 'web' },
        parse: (text) => {return  JSON.parse(text).mainDictionary },
    },
    interpolation: {
        escapeValue: false,
    }
});

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
