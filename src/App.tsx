import React from 'react';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import './App.scss';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { NotFound } from './components/SearchParams';
import { AuthProvider } from "./context";
import Payment from "./pages/Payment";
import ThreeDSecure from "./pages/ThreeDSecure";
import Summary from "./pages/Summary";
import DetailViewer from "./pages/DetailViewer";
import ErrorPage from './pages/Error';
import {CustomOfflinePayment} from "./pages/CustomOfflinePayment";

function App() {
    const theme = createMuiTheme({
        palette: {
            primary : {
                main : '#a9a9a9'
            },
            secondary: {
                main: '#FF3C66',
            },
        },
        overrides: {
            MuiCard: {
                root: {
                    minHeight: '200px',
                    // minWidth: '15px',
                    backgroundColor: '#f9f9f9'
                }
            }
        }
    });


  return (
      <>
          <ThemeProvider theme={theme}>
          <AuthProvider>
              <Router>
                  <Switch>
                      <Route exact path="/hayu/apps/payment/index.html" component={Payment} />
                      <Route exact path="/hayu/apps/management/index.html" component={DetailViewer} />
                      <Route exact path="/threeDsecure" component={ThreeDSecure} />
                      <Route exact path="/customPayment" component={CustomOfflinePayment} />
                      <Route exact path="/summary" component={Summary} />
                      <Route exact path="/error" component={ErrorPage} />
                      <Route path="/" component={NotFound}/>
                  </Switch>
              </Router>
          </AuthProvider>
          </ThemeProvider>
      </>
  );
}

export default App;
