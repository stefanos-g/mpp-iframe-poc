export const MPP_URL = "https://uat.mppglobal.com";

export const MPP_API = {
    sessions : MPP_URL + '/api/sessions',
    accounts : (accountReference: string) => `${MPP_URL}/api/accounts/${accountReference}`,
    paymentDetails : (accountReference : string) => `${MPP_URL}/api/accounts/${accountReference}/payment-details`,
    subscription: (subscriptionReference : string) => `${MPP_URL}/api/subscriptions?serviceReference=${subscriptionReference}`,
    status : `${MPP_URL}/api/alive`,
    binCheck : (card:string) => `${MPP_URL}/api/payment-details/cards?BIN=${card}`,
    addSubscription : (accountReference : string) => `${MPP_URL}/api/accounts/${accountReference}/subscriptions`,
    addCard : (accountReference : string) => `${MPP_URL}/api/accounts/${accountReference}/payment-details/card`,
    voucher : (voucherCode : string) => `${MPP_URL}/api/vouchers/${voucherCode}/validate`,
    deleteCard : (accountReference : string, paymentDetailsReference: string) => `${MPP_URL}/api/accounts/${accountReference}/payment-details/${paymentDetailsReference}`,

}

export const MPP_3DSV1 = "https://payments.mppglobal.com/interface/Mpp/ThreeDSecure/ThreeDSecureReq.aspx"

export const ACCEDO_URL = "https://api.stg.hayu.com"
