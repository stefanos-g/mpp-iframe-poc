import axios from 'axios';
import {MPP_API} from "../constants";
import {UserInfo, UserPaymentDetails, UserSession, UserSubscription, VoucherResponseInfo} from "../domain/model";
import {ActionKind} from "./reducer";

const headers = (guid : string) => {
    return {
        headers : {
            'x-sessionid' : guid,
            'x-tokenid' : '6045D1F2CDAE487CBB809E1810C40130',
            'x-version' : '11.16.0'
        }
    }
}
export async function loginUser(dispatch : any, guid : string) {
    try {
        dispatch({ type: ActionKind.RequestLogin, payload: guid });
        let response = await axios.get<UserSession>(MPP_API.sessions, headers(guid));
        let data = await response.data;

        if (data.accountReference) {
            dispatch({ type: ActionKind.LoginSuccess, payload: data });
            localStorage.setItem('currentSession', JSON.stringify(data));
            return data;
        }

        dispatch({ type: ActionKind.LoginError, error: 'Ooops' });
        console.log('Error');
        return;
    } catch (error) {
        dispatch({ type: ActionKind.LoginError, error: error });
        console.log(error);
    }
}

export async function retrieveUser(dispatch : any, guid : string, accountReference : string) {
    try {
        dispatch({ type: ActionKind.RequestUser });
        let response = await axios.get<UserInfo>(MPP_API.accounts(accountReference), headers(guid));
        let data = await response.data;

        if (data.accountReference) {
            dispatch({ type: ActionKind.RequestUserSuccess, payload: data });
            dispatch({type: ActionKind.GUIDSuccess, payload: guid });
            localStorage.setItem('currentUser', JSON.stringify(data));
            return data;
        }

        dispatch({ type: ActionKind.RequestUserError, error: 'Ooops' });
        console.log('Error');
        return;
    } catch (error) {
        dispatch({ type: ActionKind.RequestUserError, error: error });
        console.log(error);
    }
}

export async function paymentDetails(dispatch: any, guid:string, accountReference:string) {
    try {
        let response = await axios.get<UserPaymentDetails>(MPP_API.paymentDetails(accountReference), headers(guid));
        let data = await response.data;
        console.log(data);
        dispatch({ type: ActionKind.PaymentDetails, payload : data });
        return;
    } catch (error) {
        dispatch({ type: ActionKind.RequestUserError, error: error });
        console.log(error);
    }
}

export async function getSubscription(dispatch: any, guid:string, subscriptionReference: string) {
    try {
        let response = await axios.get<UserSubscription[]>(MPP_API.subscription(subscriptionReference), headers(guid));
        let data = await response.data;
        console.log(data);
        dispatch({ type: ActionKind.SubscriptionSuccess, payload : data });
    } catch (error) {
        dispatch({ type: ActionKind.RequestUserError, error: error });
    }
}

export async function validateVoucher(dispatch: any, guid:string, voucher: string) {
    try {
        let response = await axios.get<VoucherResponseInfo>(MPP_API.voucher(voucher), headers(guid));
        let data = response.data;
        dispatch({ type: ActionKind.VoucherSuccess, payload : data });
        dispatch({type: ActionKind.VoucherCodeSuccess, payload: voucher})
        return data;
    } catch (error) {
        dispatch({ type: ActionKind.RequestUserError, error: error });
        throw error.response.data;
    }
}

export async function logout(dispatch : any) {
    dispatch({ type: 'LOGOUT' });
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
}
