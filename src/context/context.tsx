import {createContext, useContext, useReducer, useState} from "react";
import {AuthReducer, UserAppState} from './reducer';

export interface Notification {
    message : string
    type : string
    location?: any
}

export type NotificationContext = {
    notification? : Notification
    sendNotification? : (notification: Notification)=>void
    onNotification? : Event
}

const AuthStateContext = createContext<UserAppState>({});
const AuthDispatchContext = createContext({});
const NotificationContext = createContext<NotificationContext>({})

export function useAuthState() {
    const context = useContext(AuthStateContext);
    if (context === undefined) {
        throw new Error("useAuthState must be used within a AuthProvider");
    }

    return context;
}

export function useAuthDispatch() {
    const context = useContext(AuthDispatchContext);
    if (context === undefined) {
        throw new Error("useAuthDispatch must be used within a AuthProvider");
    }

    return context;
}

export function useNotifications() {
    const context = useContext(NotificationContext);
    if (context === undefined) {
        throw new Error("useAuthDispatch must be used within a AuthProvider");
    }

    return context;
}

export const AuthProvider = ( {children} : any ) => {
    const [user, dispatch] = useReducer(AuthReducer, {} as UserAppState);
    const [notification,setNotification] = useState<Notification>({message:'',type: ''});

    const notificationContext : NotificationContext = {
        notification: notification,
        sendNotification: (n => setNotification(n)),
    }

    return (
        <AuthStateContext.Provider value={user}>
            <AuthDispatchContext.Provider value={dispatch}>
                <NotificationContext.Provider value={notificationContext}>
                    {children}
                </NotificationContext.Provider>
            </AuthDispatchContext.Provider>
        </AuthStateContext.Provider>
    );
};


