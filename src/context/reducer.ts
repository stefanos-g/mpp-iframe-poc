import {UserInfo, UserPaymentDetails, UserSession, UserSubscription, VoucherResponseInfo} from "../domain/model";

export enum ActionKind {
    RequestLogin,
    LoginSuccess,
    GUIDSuccess,
    LoginError,
    RequestUser,
    RequestUserSuccess,
    RequestUserError,
    PaymentDetails,
    SubscriptionSuccess,
    VoucherSuccess,
    VoucherCodeSuccess
}

export type PayloadKind = UserSession | UserInfo | UserPaymentDetails | UserSubscription[] | VoucherResponseInfo | string;

export type ActionType = {
    type : ActionKind
    payload : PayloadKind
}

export interface UserAppState {
    session? : UserSession
    user?: UserInfo
    paymentDetails? : UserPaymentDetails
    subscription? : UserSubscription
    guid?: string
    voucher?: VoucherResponseInfo
    voucherCode?: string
}

export const AuthReducer = (initialState : UserAppState, action : ActionType) => {
    switch (action.type) {
        case ActionKind.RequestLogin:
            return {
                ...initialState,
            } as UserAppState;
        case ActionKind.LoginSuccess:
            return {
                ...initialState,
                session: action.payload,
            } as UserAppState;
        case ActionKind.GUIDSuccess:
            return {
                ...initialState,
                guid: action.payload,
            } as UserAppState;
        case ActionKind.LoginError:
            return {
                ...initialState,
            } as UserAppState;

        case ActionKind.RequestUser:
            return {
                ...initialState
            } as UserAppState
        case ActionKind.RequestUserSuccess:
            return {
                ...initialState,
                user: action.payload
            } as UserAppState;
        case ActionKind.RequestUserError:{
            return {
                ...initialState
            } as UserAppState
        }
        case ActionKind.PaymentDetails: {
            return {
                ...initialState,
                paymentDetails : action.payload
            } as UserAppState
        }
        case ActionKind.SubscriptionSuccess: {
            return {
                ...initialState,
                subscription : (action.payload as UserSubscription[])[0]
            } as UserAppState
        }
        case ActionKind.VoucherSuccess: {
            return {
                ...initialState,
                voucher: (action.payload as VoucherResponseInfo)
            }
        }
        case ActionKind.VoucherCodeSuccess: {
            return {
                ...initialState,
                voucherCode: (action.payload as string)
            }
        }
        default:
            throw new Error(`Unhandled action type: ${action.type}`);
    }
};
